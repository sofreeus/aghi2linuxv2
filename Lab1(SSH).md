---

## Lab 1 Connect to your server

#### Lab Objectives:

1. Create an SSH Keypair
2. Copy the public key to your server
3. Conncet to your server


***Note: This lab does not cover puTTy. If any users are utilizing it, we'll have a small breakout on how to do this.***

_Create a SSH Keypair and log into your assigned machine_

1. In your terminal, create an SSH Key with the command `ssh-keygen`
2. Select the default location by hitting `[ENTER]`
3. Choose a password for your key and enter it a second time when prompted.
4. This will display a fingerprint and randomart image

_Copy your public key to your assigned server_

1. In your terminal, run the command `ssh-copy-id <username>@<IP Address>`

_Log into your server_

1. It's now time to access your assigned server. Log in with `ssh <username@<IP Address>`
 * note: If you are asked for a password, check your username and/or make sure you copied your ssh key properly.

 ---

 ## Creating your key with puTTy and copying it onto your server

https://devops.ionos.com/tutorials/use-ssh-keys-with-putty-on-windows/
