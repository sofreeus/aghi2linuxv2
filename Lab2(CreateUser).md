---

## Lab 2 Create your user

#### Lab Objectives:

1. Create a user
2. Copy your public key to the new user's directory
3. Add the user to the sudoers file
4. Reconnect as the new user
5. Disable the old user

_Create your new user_

`adduser` vs `useradd`

`useradd newuser`

* is a basic command and only adds a user. It does not set the user password.
* requires switches to set shell and home directory.
* is available on all distros

`adduser`

* is frequently a script to create the users
* interactive
* Copies /etc/skel
* Available on most Ubuntu Based Distros

1. Enter the following command into your command prompt: `adduser <username>`
2. Follow prompts to complete

_Copy your public key to the new user's directory_

1. Make the .ssh directory for the new user `mkdir /home/<username>/.ssh/`
2. Copy over the authorized_keys file. `cp ./.ssh/authroized_keys /home/<username>/.ssh/`
3. Change the ownership of the file. `chown newuser:newuser /home/<username>/.ssh/authorized_keys`


_Add the users to the sudoers file_

1. Lets open the sudoers file. `visudo`
2. Scroll down to the section of the sudoers file that says `#User privilege specification`
3. Add a line to the file like this: `<username>  ALL=(ALL:ALL) NOPASSWD:ALL`
4. Press `[CTRL]+x` to exit
5. Press `y` to save buffer
6. Press `[ENTER]` to complete


##### A couple of notes on what we just did

* It is possible to limit a sudo user to specific commands. The `ALL=(ALL:ALL) ALL` portion of the line breaks down as follows:

  * The first ALL determines which hosts can be sudo'd from.
  * The Second ALL is the users that the sudoer can sudo as.
  * The Third ALL is the user groups that the sudoer can sudo as.
  * The Fourth ALL is the commands that a user can do when sudoing.

For example: `192.168.0.1=(newuser:newuser) useradd` would limit the defined user to the host of 192.168.0.1 and could only sudo the `useradd` command as the user `newuser`

* You can also add groups to the sudoers file. The line `%sudo  ALL=(ALL:ALL) ALL` in your current sudoers file allows any user in the group `sudo` to use the sudo command. You can use the command `sudo usermod -a -G sudo <username>` to add a user to the `sudo` group.

_Reconnect as the new user_

1. Execute the command `exit` or press `[CTRL]+d` to disconnect from the default user.
2. Reconnect to the remote server. `ssh <newuser>@<IP Address>`

_Disable the original user_

Here we are going to confirm that sudo works for your user and disable the original user at the same time. This can be done with a single command:

```bash
sudo usermod -L <old_username>
```

##### An explanation of how this works.

Linux stores user passwords in the /etc/shadow file. You can view this file with `sudo cat /etc/shadow`. And example entry is below:

```bash
newuser:$6$riy5K/wkRP8UEGiH$Mr5SSixVrMhy.5vMqVxByiB1KnluPtki5R3bQzNxzxDtD3SFR/uTiSN41uoOrVz7LUzx3uLqg.5vnTIw7yRgJ0:18955:0:99999:7:::
```

What you are seeing is the username, password, userid, and more seperated with `:`. When you run the command above, you're adding a `!` to the begining of the second field which is the users hashed password. You can manually add a `!` to the users password hash by directly modifying the /etc/shadow file instead of using the command above.


_Create a common user across all machines_

Now follow the beginning steps above to create another user named `class` with the password `Freedom-13`. There is no need to add the ability to sudo.
