---


## About Linux

* An Operating System
* Libre and subject to the Four Freedoms
* Both a Desktop and a Server
* Wide Variety of Desktop Environments

---

## Scope

* Learn the basics of the CLI (Command Line Interface)
* Learn how to install applications
* Modify configurations

#### This is not a class on Linux as a Desktop

---

## Distributions

* CentOS
* Fedora
* Debian
* Ubuntu
* DistroWatch.com

#### We'll be using Ubuntu for this class
