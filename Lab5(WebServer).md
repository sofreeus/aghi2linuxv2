## Lab 5 Let's make a webserver!

Linux is the most popular webserver on the planet. Let's try a few common operations.

credit: https://upcloud.com/community/tutorials/installing-lamp-stack-ubuntu/


### Install the Apache2 httpd server

```bash
sudo apt update
sudo apt install apache2
```

---

### Make a simple "Hello, World!" page

1. What is my IP address?
2. Browse it.
3. Replace the default page.
```bash
echo '<html><body><h1>Hello, World!</h1></body></html>' | sudo tee /var/www/html/index.html
```
4. Browse it again.

---

### Add PHP

```bash
sudo apt install php7.4-mysql libapache2-mod-php7.4
echo '<?php phpinfo(); ?>' | sudo tee /var/www/html/index.php
sudo rm /var/www/html/index.html
```

And browse again. If the PHP install worked, you'll see PHP diags.

---

### Add MySQL

```bash
sudo apt install mariadb-server mariadb-client
sudo mysql_secure_installation
```
