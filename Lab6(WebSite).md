## Lab 6 Now lets make a full blown website with Joomla

Joomla is a powerful open source content management system like Wordpress.

This is going to take some extra packages.

1. Copy this command into your CLI to install the extra packages

```bash
sudo apt install libapache2-mod-php7.4 openssl php-imagick php7.4-common php7.4-curl php7.4-gd php7.4-imap php7.4-intl php7.4-json php7.4-ldap php7.4-mbstring php7.4-mysql php7.4-pgsql php-ssh2 php7.4-sqlite3 php7.4-xml php7.4-zip
```

note: 7.2 -> 7.4 and removed php-smbclient

Now lets create the database for Joomla to use.

1. `mysql -u root -p` 
2. At the `mysql>` prompt run the following:

```sql
CREATE DATABASE joomla;
CREATE USER joomla_user@localhost IDENTIFIED BY '<password>';
GRANT ALL ON joomla.* TO 'joomla_user'@'localhost';
FLUSH PRIVILEGES;
EXIT;
```

Next we are going to download and install Joomla

```bash
wget https://downloads.joomla.org/cms/joomla3/3-9-26/Joomla_3-9-26-Stable-Full_Package.zip
sudo mkdir /var/www/html/joomla
sudo unzip Joomla*.zip -d /var/www/html/joomla
# might need sudo apt install unzip
sudo chown -R www-data:www-data /var/www/html/joomla
sudo chmod -R 755 /var/www/html/joomla
```

Now to configure apache2 to serve up Joomla

1. `sudo vim /etc/apache2/sites-available/joomla.conf`
2. Add the following to the file:

```bash
<VirtualHost *:80>
     ServerAdmin garomero@sofree.us
     DocumentRoot /var/www/html/joomla/
     ServerName sofree.us
     ServerAlias name.sofree.us

     ErrorLog ${APACHE_LOG_DIR}/error.log
     CustomLog ${APACHE_LOG_DIR}/access.log combined

     <Directory /var/www/html/joomla/>
            Options FollowSymlinks
            AllowOverride All
            Require all granted
     </Directory>
</VirtualHost>

```

3. Exit vim with `[ESC]` then `[Shift]+Z` twice
4. Now tell apache2 to recognize and activate the site.

```bash
sudo a2ensite joomla.conf
sudo a2enmod rewrite
sudo systemctl restart apache2
```

Now we complete the Joomla installation from a browser.

1. Navigate a web browser to http://studentname.sofree.us
2. Enter the information requested on the page, make note of the superuser infor and click Next
3. It's going to ask for your Database configuration informaiton including your password from above then click Next.
4. Next page is a review. Note anything not green!!!! If all is good, click Install
5. At the bottom of the page, click the "Remove Installation Folder"
6. Now click "Administer" and login with the superuser account you created in step 2.
